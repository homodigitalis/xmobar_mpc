extern crate regex;

use std::process::Command;
use regex::Regex;

fn resolve_status(status: &str) -> () {
    if status == "[playing]" {
        print!(">> ");
    } else if status == "[paused]" {
        print!("|| ");
    } else {
        print!("");
    }
}

fn main() {
    let output = Command::new("mpc")
        .arg("status")
        .output()
        .expect("");

    let output_formatted = String::from_utf8_lossy(&output.stdout);

    let re_status = Regex::new(r"\[(paused|playing)\]").unwrap();
    match re_status.captures(&output_formatted) {
        Some(caps) => resolve_status(&caps[0]),
        None => print!("")
    }

    let split = output_formatted.split("\n");
    let vec: Vec<&str> = split.collect();

    // Remove the .contains() check for
    // the MPC playback parameters output
    // (volume, repeat, random, single, consume)
    // when MPC is stopped.
    if vec[0].contains("volume") {
       print!("")
    } else {
       print!("{} ", &vec[0]);
    }

    let re_time = Regex::new(r"([\d:]{1,}/[\d:]{1,})").unwrap();
    match re_time.captures_iter(&output_formatted).nth(1) {
        Some(caps) => print!("[{}] |", caps.get(0).unwrap().as_str()),
        None => print!("")
    };
}
